<!-- vim: set spelllang=es : -->

Una de las herramientas más poderosas que tenemos a nuestra disposición son nuestros teléfonos inteligentes conectados a Internet.
La comunicación instantánea, sumada al hecho de tener todo el conocimiento humano al alcance de la mano, aumenta de manera masiva nuestra capacidad de afectar al mundo que nos rodea.
Sin embargo, esta conectividad se produce a costa de una mayor vigilancia por parte de los aparatos de seguridad del Estado y de los particulares.
Las personas activas en movimientos de liberación son conscientes, en diferente medida, de esta vigilancia.
Por esta razón, hemos desarrollado Prácticas de Seguridad Operacional (OpSec) y una cultura de seguridad interna para contrarrestar las interrupciones de nuestros esfuerzos organizativos.

Hay muchas leyendas urbanas en torno a los celulares.
Por lo general, estos relatos se derivan de la falta de comprensión de la tecnología telefónica y de las capacidades que el Estado y los actores privados
tienen a su disposición para vigilar a los individuos a través de sus celulares.
El "modelado de amenazas" es el proceso de identificar las amenazas y construir contramedidas pragmáticas y específicas contra ellas.
Sin embargo, sin modelos precisos de los adversarios, los modelos pueden conducir a contramedidas ineficaces.
Las acciones basadas en información errónea pueden provocar arrestos fáciles o crear la impresión de un adversario omnividente, lo cual dificulta la acción.
Este texto cubre la tecnología básica de los celulares y aborda las leyendas urbanas populares para que tú y tus compañeros puedan resistirse a la interrupción y organizarse de forma eficaz.

No existe la seguridad perfecta.
No es una dualidad entre "seguridad" y "no seguridad," ni tampoco es un espectro entre "mejor seguridad" y "peor seguridad."
La seguridad depende tanto de las medidas preventivas como de las circunstancias.
Y, lo que puede ser útil para evitar que el Estado no te rastree tal vez no sea útil para prevenir que una pareja abusiva lea tus mensajes.

Esta guía te ayudará a comprender los riesgos a los que te enfrentas para que puedas tomar decisiones informadas.
La cultura de seguridad no
es una garantía, pero puede reducir los riesgos.
Podría prevenir tu encarcelamiento, así como salvar tu vida, o la de los que te rodean.

Este texto fue escrito a comienzos de 2022 por anarquistas en Europa y Norteamérica y, por ello, será más relevante para aquellas personas que están cerca de nosotros tanto en el espacio como en el tiempo.
Omitimos la mayoría de la información legal; solo porque tu adversario no tenga permitido hacer algo no significa que no lo vaya a hacer de todas formas.
Por esta razón, abordamos las posibilidades. También reconocemos que no somos capaces de predecir el futuro.
Vas a tener que usar la familiaridad de tu contexto personal y local para adaptar lo que
escribimos a las amenazas específicas que enfrentas.
