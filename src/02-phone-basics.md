<!-- vim: set spelllang=es : -->

# Tú y tu teléfono

Tu teléfono[^phone] no es sólo una pertenencia personal valiosa, sino que también constituye una extensión de ti mismo.
Contiene tus memorias, tus conocimientos y tus pensamientos privados y semiprivados.
Te permite buscar información de manera rápida y compartirla con los demás.

[^phone]: Para ahorrar espacio, usamos "teléfono" para referirnos a "teléfono móvil" o "teléfono celular."

Esta conectividad, junto con el acceso al conocimiento, nos hace más efectivos a la hora de perseguir nuestros objetivos.
Los teléfonos se han convertido, hasta cierto punto, en una necesidad para funcionar en la sociedad moderna.
Por ello, las personas casi siempre los llevamos encima.
Además nos pasa que cuando salimos de casa sin él o se le agota la batería, casi todos nos sentimos desnudos e impotentes, como si nos faltara una parte de nosotros.

El compromiso de un teléfono por un adversario, ya sea por medio de confiscación o *malware*, puede ser desastroso.
Todos tus mensajes, imágenes, correos electrónicos y apuntes pueden quedar a disposición de dicho adversario.
Además, podrían acceder a todas las cuentas en tu teléfono.
El *malware* o las aplicaciones de acecho podrían activar tu micrófono o el rastreo de tu ubicación en tiempo real.

Además de estos tipos de vigilancia activa, tu teléfono proporciona vigilancia pasiva a partes privilegiadas.
Por ejemplo, la policía puede solicitar acceso en tiempo real o acceso masivo a los metadatos que están disponibles para tu operador o PSI.[^isp]

Ante estas posibilidades de vigilancia, los activistas sostienen, con toda razón, que "tu teléfono es un poli" y que "tu teléfono es un soplón."
Entonces, ¿debemos seguir usando nuestros teléfonos por lo que nos proporcionan o debemos deshacernos de ellos por los riesgos que suponen?
O quizás haya algún matiz acerca de cuándo y cómo podemos usar los teléfonos de modo que se nos permita conservar buena parte de los beneficios y eludir la mayoría de los detrimentos.

[^isp]: Proveedor de servicios de Internet
