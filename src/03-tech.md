<!-- vim: set spelllang=es : -->

# Tecnología telefónica

Para entender cómo los teléfonos pueden ser pinchados para la vigilancia, necesitamos tener una comprensión sólida de las diferentes tecnologías telefónicas. Necesitamos comprender el funcionamiento del *hardware*, el *firmware*, el sistema operativo, las redes móviles y, en cierta medida, la Internet en general.
Esto te ayudará a construir un modelado de amenazas para que puedas tomar decisiones informadas, lo cual es preferible en lugar de memorizar pasos arbitrarios.

## Redes celulares

Las redes celulares toman su nombre de las varias celdas superpuestas de
cobertura proporcionadas por las torres transceptoras.[^cell-tower].
La cobertura es más densa en zonas urbanas, por lo que un solo teléfono  está en contacto con más torres.
En zonas suburbanas y rurales, hay menos solapamiento de cobertura y, consecuentemente, un teléfono está en contacto con menos torres.

[^cell-tower]: No todos los emplazamientos celulares son torres, pero basta con usar el término común.

Los operadores de redes pueden usar información acerca de la propia
señal para estimar la ubicación de los teléfonos.
La ubicación imprecisa puede determinarse por el ángulo de llegada a la torre o por el conocimiento sobre desde qué sector[^sector] llegó la señal.
Cuando se mide la distancia de un teléfono a varias torres, el proveedor de la red puede triangular la ubicación del teléfono con gran precisión.[^multilateration].
Las redes LTE pueden situar la posición de un teléfono con una precisión de unas decenas de metros, y las redes 5G son capaces de hacerlo con una precisión de cinco metros. Cuántas más torres haya, más fiable será la localización de un teléfono y, por lo tanto, la triangulación rural suele ser menos precisa que la urbana.

[^sector]: El área en forma de cono cubierta por una sola antena.

[^multilateration]: Esto se llama "multilateración de enlace ascendente."
    En este caso, usamos \"triangulación\" para referirnos a "multilateración" porque vale la pena cambiar la terminología técnica por la comprensibilidad.

Cuando los teléfonos se conectan a una red celular, envían una identificación de dispositivo único (IMEI[^imei]) junto con su identificación de abonado (IMSI[^imsi]).
El IMSI suele almacenarse en una tarjeta SIM[^sim] o en una eSIM.[^esim]
Esto significa que el intercambio de varias tarjetas SIM entre un dispositivo o el intercambio de una tarjeta SIM entre varios dispositivos puede crear un enlace duro entre esas identidades.
No se requiere una SIM o una IMSI para hacer una llamada; estas sólo autentican el dispositivo ante el operador y determinan si el aparato está autorizado a hacer llamadas o a utilizar los datos móviles.
Por ejemplo, en todas (o en casi todas) las regiones se puede llamar a los servicios de emergencia sin necesidad de una SIM. Retirar una tarjeta SIM del teléfono **no impide el rastreo.**

[^imei]: Identidad Internacional de Equipo Móvil.
[^imsi]: Identidad de Suscriptor Móvil Internacional.
[^sim]:  Módulo de Identificación de Abonado.
[^esim]:  SIM integrada, un chip integrado directamente en el dispositivo.

## Tipos de teléfonos

La mayoría de la gente que dice "teléfono" quiere decir "teléfono inteligente", es decir, uno con un sistema operativo y aplicaciones que pueden ser instaladas por los usuarios.
Un teléfono con funciones básicas es el tipo menos sofisticado de teléfono móvil, como los que se veían en los viejos tiempos y que sólo pueden hacer llamadas telefónicas y enviar mensajes SMS.

Los teléfonos *feature* son algo raros hoy en día.
Están a medio camino entre los teléfonos inteligentes y los teléfonos sencillos.
Pueden tener aplicaciones específicas del proveedor, tales como una *app *de correo electrónico o un navegador de Internet incorporado.
Para distinguir los teléfonos *feature* y aquellos con funciones básicas de los teléfonos inteligentes, se usa el término "teléfono sencillo" para describir los dos primeros tipos.[^dumb-phones]

[^dumb-phones]: A veces, se usa el termino "teléfono tonto" para referirse o a todos los telefonos sencillos o solamente a los teléfonos basicos.
En aras de la claridad, evitamos usar este término.

### Teléfonos inteligentes

Los teléfonos inteligentes suelen tener un servicio de localización que permite a los teléfonos proporcionar datos de ubicación de gran precisión y en tiempo real a las aplicaciones, en particular a los mapas.
El servicio de localización utiliza señales recibidas de los satélites GPS[^gps] o GLONASS[^glonass] para triangular la ubicación del dispositivo.
La mayoría de los teléfonos inteligentes utilizan A-GPS.[^agps]
A-GPS combina las señales de las torres, las señales de wifi e incluso los datos intercambiados a través de la Internet para calcular de manera rápida y precisa la posición del dispositivo.

[^gps]: Sistema de Posicionamiento Global.
[^glonass]: Sistema Global de Navegación por Satélite.
[^agps]: GPS asistido.

Los teléfonos inteligentes contienen frecuentemente una brújula, un acelerómetro, un giroscopio y un barómetro.
Incluso sin GPS o multilateración, las mediciones de esos sensores pueden combinarse para obtener una ubicación actual aproximada desde una ubicación anteriormente conocida.

Esto significa que pese a que las señales GPS son recibidas por el dispositivo de manera pasiva, el uso de los servicios de ubicación puede dar a conocer la posición del teléfono.
Esto también evidencia que desactivar las funciones de localización tal vez no sea suficiente para evitar que un *malware* o una aplicación no deseada encuentre la ubicación de tu teléfono de manera aproximada.

### Teléfonos sencillos

Muchos activistas creen que usar teléfonos sencillos en lugar de teléfonos inteligentes es "más seguro."
Dado que un teléfono sin GPS ni servicios de localización todavía puede ser geolocalizado, los móviles sencillos no ofrecen una protección significativa contra el rastreo de ubicación.
Los teléfonos *feature* típicamente carecen de las aplicaciones de chat de voz o texto, y, por definición, los teléfonos con funciones básicas no tienen esas capacidades.
Esto significa que sólo se dispone de SMS y llamadas telefónicas sin cifrar, y estas son susceptibles a ser interceptadas de más maneras que si hubiera un modelo cliente-servidor o un cifrado de extremo a extremo.
Los teléfonos con funciones básicas, los que parecen menos avanzados tecnológicamente, solo pueden tener capacidades 2G.
Esto significa que las llamadas y los textos SMS pueden ser interceptados fácilmente usando productos no profesionales por un valor de 25 euros.
Y, además, muchos de estos dispositivos podrían tener capacidades ocultas de Internet que envían datos telemétricos de vuelta a los fabricantes sin el conocimiento del usuario.

En resumen, **los teléfonos sencillos no son más seguros** que los teléfonos inteligentes contra la mayoría de las amenazas enfrentadas por los activistas.

## Malware

El *malware* es un *software* malicioso.
Es un programa que hace cosas que no quieres que haga mientras que, al mismo tiempo, intenta ocultar sus actividades.
El *malware *creado por el Estado tiene el objetivo de vigilar y extenderse a otros teléfonos o incluso a dispositivos electrónicos como los *routers* de wifi.

Viejas recomendaciones sobre seguridad decían que el *malware* se instalaba al visitar páginas sospechosas o abrir adjuntos en correos electrónicos de remitentes desconocidos y, aunque esto sigue siendo verdad, la superficie de ataque es mucho más grande.
La mayoría de las aplicaciones (por no decir todas) esperan a que lleguen las notificaciones de los servicios de Google Play y luego hacen peticiones a los servidores de la *app*.
Algunos de los *malwares* son de clic cero, o sea, no requieren la interacción del usuario.
Por ejemplo, el *spyware* Pegasus de NSO Group utilizó un virus de clic cero para atacar a activistas, periodistas y políticos.
El *malware* puede instalarse en tu teléfono aunque solo uses aplicaciones de confianza y solo aceptes a sabiendas mensajes de contactos conocidos.

Algunos casos de *malware* solo permanecen en la memoria de tu teléfono mientras están encendidos y son incapaces de persistir entre reinicios.
Debido a esto, algunos tipos de *malware *piratean las rutinas de apagado de tu teléfono y realizan un falso apagado.
Aun así, reiniciar tu teléfono podría potencialmente eliminar un *malware*.
Dicho esto, si crees que tu dispositivo ha sido infectado, tendrás que encontrar un especialista de *malware* que te pueda ayudar a determinarlo.
También es posible que vayas a necesitar un nuevo teléfono.
El *malware* es menos común de lo que se cree, pero no dejes que su rareza te haga ignorar las señales de advertencia.
El *malware* creado por el Estado no se detecta tan fácilmente como el *malware* de bajo esfuerzo, por lo que los métodos comunes pueden no ser aplicables.

Lamentablemente, la detección no es algo que puedas hacer por tu cuenta.

## Sistemas operativos

Una de las preguntas más comunes que se hacen los activistas con respecto a los teléfonos inteligentes es la siguiente: "¿Qué es más seguro, iOS o Android?"
Como con todas las otras preguntas de seguridad, la respuesta es "depende."

Los sistemas operativos de los teléfonos inteligentes son de dos tipos: iOS para dispositivos Apple, y Android para todo lo demás.
iOS es propietario y tiene un "código fuente privado."
Android es un sistema operativo base que tiene un código fuente público que los fabricantes pueden modificar para sus dispositivos.
Los sistemas operativos Android de los fabricantes suelen tener un código fuente privado.
Además, hay muchas versiones completas de Android mantenidas por la comunidad de código abierto, sobre todo LineageOS[^cyanogen].
GrapheneOS y CalyxOS son sistemas operativos Android de código abierto que se centran en la privacidad y la seguridad.

[^cyanogen]: LineageOS es el sucesor popular del descontinuado CyanogenMod.

Cuando un teléfono está encendido, el *hardware* empieza a cargar el sistema operativo a través de un proceso en el que cada paso verifica la integridad del *software* necesario para el siguiente paso.
Este proceso se llama "arranque seguro."
Para instalar un sistema operativo personalizado, el arranque seguro debe ser desactivado.
De no ser así, el *hardware* se negaría a cargar el sistema operativo personalizado.
La razón de esto radica en que el *hardware *no está firmado criptográficamente por una llave confidencial incluida por el fabricante.
Esto permite la posibilidad de que se instale un sistema operativo malicioso que pueda leer los datos a través de *malware*, en lugar de un verdadero sistema operativo .
Sin embargo, esto no significa que los sistemas operativos integrados (los sistemas operativos precargados en tu dispositivo por defecto) sean más o menos seguros que aquellos personalizados.
Evidencia que hay un perfil de riesgo diferente al desactivar el arranque seguro y usar un sistema operativo personalizado.

Cuando se desarrolla un *malware*, este debe dirigirse a una sola aplicación o sistema operativo.
El desarrollo del *malware *cuesta tiempo y dinero, y, una vez que se implementa, puede ser detectado y volverse incapaz de infectar nuevos dispositivos a causa de actualizaciones de la aplicación o del sistema operativo.[^malware-reuse]
Por esta razón, resulta más económico codificar *malware* que pueda atacar a muchos usuarios.
iOS cuenta con un número limitado de versiones para un número limitado de dispositivos, mientras que el ecosistema

[^malware-reuse]: Además, el *malware* tiene la interesante propiedad de que, cuando se utiliza, puede ser capturado y clonado para que otro puedan reutilizarlo.
Sería como si existiera la posibilidad de que los misiles pudieran ser copiados y recreados de manera instantánea e infinita cada vez que un misil cayera en territorio enemigo; y también como si ese tipo concreto de misil tuviera muchas más probabilidades de ser interceptado en el futuro.
Los ejércitos dudarían en disparar tantos misiles y tendrían que ser mucho más estratégicos en sus objetivos.

Android es mucho más diverso.
Esto significa que crear *malware* dirigido a los usuarios de Android es menos económico y más difícil para los adversarios.

Nuestras recomendaciones son las siguientes:

- Para la mayoría de los individuos que tratan de evitar la vigilancia masiva y los *hackers* de bajo esfuerzo, iOS o Android estándares son suficientes puesto que son más fáciles de usar.
- Para las personas significamente involucradas en movimientos sociales o personas que esperan ser apuntadas, recomendamos para su organización y trabajo político que usen GrapheneOS sin servicios de Google Play, que usen F-Droid como único repositorio de aplicaciones y que instalen solo el número mínimo de aplicaciones necesarias para las comunicaciones.
- Para las personas que han atraído o esperan atraer la atención de las agencias de inteligencia, se deben evitar los teléfonos para todo lo relacionado con el activismo.

## Cifrado de dispositivos

iOS y Android ofrecen la posibilidad de cifrar tus datos personales.
Esto recibe diferentes nombres, tales como Protección de Datos y Cifrado de Dispositivos.
Generalmente, los teléfonos **no tienen activado** el cifrado de dispositivos por defecto.
Esta función **debe ser activada** por el usuario, ya sea al configurar el teléfono o, luego, a través de los ajustes.
Asimismo, la protección contra repetidos intentos de inicio de sesión debe ser activada.

La implementación de cifrado de dispositivos suele utilizar un módulo de seguridad de *hardware* (HSM) o un co-procesador de seguridad,[^secure-enclave] o sea, *chips *especiales en el teléfono que se encargan del cifrado, el descifrado y las claves criptográficas utilizadas para estas operaciones.
Estos *chips* son importantes porque protegen las claves de la manipulación o del acceso no autorizado.
También pueden impedir que tus adversarios accedan tus datos, pero no es una garantía.
La herramienta GrayKey, entre otras, puede aprovecharse de los *bugs* en los HSM y, en algunos casos, puede *hackear* la contraseña de desbloqueo y descifrar los datos.
Aunque los HSM pueden estar seguros ahora, puede que se descubran nuevos *bugs* el mes que viene y las autoridades también pueden desarrollar nuevas técnicas para recuperar datos dentro de 5 o 10 años.

[^secure-enclave]:  En los dispositivos de Apple, este chip se llama Enclave Seguro.

El cifrado de dispositivo es bueno para prevenir que tus datos sean accesados si un ladrón roba tu teléfono o si un policía te lo quita durante una detención.
No es probable que resista los esfuerzos concertados para acceder a tus datos por parte de agencias de inteligencia estatales como el MI5 o el FBI.

Un ejemplo destacado es cuando el FBI descifró la contraseña del teléfono del tirador masivo alrededor de un año después del tiroteo de San Bernardo en 2015. Unos cinco años después, se reveló que el acceso a los datos fue realizado a través de una serie de vulnerabilidades contra el *software* en el HSM.

El uso del cifrado de dispositivo puede proteger contra la captura de datos, pero **la única forma de garantizar que los datos no caigan en manos de las autoridades es asegurando que esos datos nunca hayan existido**.

## VPNs

Una red privada virtual (VPN), en el uso más común del término, se refiere a una aplicación que enruta el tráfico de Internet de un dispositivo a un servicio cuyo propósito es ocultar el tráfico *web* y la dirección IP de los observadores de la red o de los servidores a los que se está conectando.
Cuando se usan, las VPNs protegen tu tráfico de ser controlado a través de redes wifi públicas y, al mismo tiempo, esconden tu dirección IP de los servidores a los que se ha conectado.

Las aplicaciones VPN pueden causar distracciones durante una investigación o dificultar la vigilancia pasiva, pero también uno puede olvidarse de permitirlas o puede haber un escape de tráfico.
El tráfico hacia y desde tu proveedor de VPN puede ser obtenido por las agencias de inteligencia estatales que son capaces de ver todo el tráfico de Internet.
Y, además, tu servicio de VPN puede ser legalmente obligado a recopilar registros y entregarlos a las autoridades.
Las VPNs son baratas y pueden mejorar la seguridad en algunos aspectos, pero no se debe confiar en ellas para proporcionar anonimato contra el Estado.

## IMSI catchers

Un IMSI *catcher*[^imsi-catcher] es un dispositivo que se disfraza como una torre celular legítima, lo que obliga a los teléfonos a conectarse a él, permitiendo así la escucha telefónica o el envío de mensajes o llamadas telefónicas.
A veces, esta "suplantación" se puede detectar, pero no se debe confiar por completo en la detección.
En algunas regiones, los IMSI *catchers* pueden ser usados sin orden judicial, especialmente durante las manifestaciones.
Funcionan en parte al degradar los protocolos de seguridad a unos sin cifrado o con cifrado débil.
Aunque los teléfonos inteligentes prefieren protocolos que ofrecen más protección contra la interceptación y la suplantación, los IMSI *catchers* todavía pueden degradar los teléfonos a protocolos inseguros.
Con respecto a esto, las llamadas telefónicas y los mensajes SMS enviados y recibidos por los teléfonos inteligentes no son lo suficientemente resistentes contra la interceptación.

[^imsi-catcher]: También conocido como Stingrays después de una marca popular de ellos.

## Bolsas de Faraday

Los teléfonos envían y reciben información mediante la radiación electromagnética.
Esta radiación puede ser bloqueada por materiales especiales.
Las leyendas urbanas y algunas evidencias dicen que las señales se pueden bloquear metiendo un teléfono en una o varias bolsas forradas de aluminio.
Sin embargo, al igual que muchas otras contramedidas, no se debe depender de esto.
Se puede confiar en las bolsas de Faraday para bloquear las señales telefónicas.
Si necesitas transportar los teléfonos y asegurar que las señales no se filtren, apagarlos tal vez no sea suficiente.
A pocos teléfonos inteligentes se les puede quitar la batería, y algo en tu bolsa rozando tu teléfono puede presionar el botón de encendido.
El malware puede piratear las rutinas de apagado y prevenir que el teléfono se apague al intenta apagarlo.
Colocar un teléfono apagado en una bolsa de Faraday puede reducir considerablemente las probabilidades de que se determine tu ubicación.
