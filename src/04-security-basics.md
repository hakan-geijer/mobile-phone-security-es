<!-- vim: set spelllang=es : -->

# Elementos básicos de seguridad

Hay algunas prácticas telefónicas que son recomendables para la mayoría de los activistas.
Algunas de ellas son las siguientes:

## Actualizaciones

No cabe duda de que lo mejor que puedes hacer para evitar ser hackeado por las autoridades o por un hacker cualquiera es actualizar rápidamente el sistema operativo y las apps de tu teléfono.
Puede ser molesto tener que actualizarlo todo el tiempo, pero las actualizaciones contienen parches de seguridad para las vulnerabilidades críticas.
Y, además, esto puede evitar que se vacíen tus cuentas bancarias, de pago, y de transferencia.

## Gestores de contraseñas

La segunda práctica de seguridad más útil es usar un gestor de contraseñas para todas tus cuentas, incluidas las usadas en tu teléfono.
Hay versiones de pago que permiten la sincronización automática de las contraseñas de diferentes dispositivos y de los inicios de sesión automáticos a sitios web.
Sin embargo, estas requieren un cierto nivel de confianza en la corporación que vende el producto.
Existen alternativas gratuitas como KeePassX, pero no tienen la maniobrabilidad de los productos de pago.
Al usar un gestor de contraseñas, todas tus cuentas deben tener contraseñas seguras, únicas, y aleatorias.
Estas suelen ser generadas automáticamente por el gestor.
La contraseña maestra para desbloquear el gestor debe ser una frase larga y aleatoria.

Los humanos son notoriamente malos en generar la aleatoriedad necesaria para las contraseñas.
Usar la primera línea de tu poema preferido o alterar los caracteres de antifascismo a `an7if4sc1sm0!` puede ser fácilmente descifrado por los computadores.
Diceware es un método para generar contraseñas tirando los dados y usarlos para seleccionar palabras de una lista predefinida.
Cinco palabras es lo mínimo, seis es mejor, pero cualquier número más de ocho es excesivo.
Hacer esto proporciona una aleatoriedad indescifrable que no se puede crear por cuenta propia, y, por otra parte, esta aleatoriedad es fácil de recordar.
La EFF proporciona una lista de palabras fácil de usar en inglés.
La lista de palabras en español disponible del sitio web de A. G. Reinhold tiene algunas palabras difíciles de recordar, pero aún así puede ser más fácil de usar para los hispanohablantes que la lista de la EFF.
Una frase de ejemplo de la lista de EFF es `MutableCalmBlubberFitJustify` (por favor **no uses esta**, crea la tuya propia).

: Muestra de Diceware del EFF

+-------------+-----------+--------------+
| **Números** | **EFF**   | **Original** |
+=============+===========+==============+
| 24311       | drowsily  | baste        |
+-------------+-----------+--------------+
| 24312       | drudge    | basto        |
+-------------+-----------+--------------+
| 24313       | drum      | q92          |
+-------------+-----------+--------------+
| 24314       | dry       | basura       |
+-------------+-----------+--------------+
| 24315       | dubbed    | bata         |
+-------------+-----------+--------------+
| 24316       | dubiously | q91          |
+-------------+-----------+--------------+
| 24321       | duchess   | batel        |
+-------------+-----------+--------------+
| 24322       | duckbill  | bateo        |
+-------------+-----------+--------------+

## Bloquear tu teléfono

Dependiendo de tu modelo de amenazas, tal vez te convenga hacer que sea difícil (o casi imposible) desbloquear tu teléfono.
Esto tiene una importancia especial porque el método de desbloqueo es también el método de descifrado, por lo que un método de desbloqueo seguro ayuda a proteger contra el acceso no deseado a tus datos si se confisca tu teléfono.
Generalmente, debes preferir las contraseñas por encima de los códigos PIN o los patrones, porque el primero es más difícil de descifrar por las máquinas.
Probablemente deberías desactivar la función de desbloqueo facial, y quizás debas desactivar el desbloqueo de huella dactilar.
En algunas regiones existen protecciones legales para las contraseñas pero no para las huellas u otros biométricos.

Algunos teléfonos ofrecen la capacidad de eliminar todos los datos si se han realizado demasiados intentos de inicio de sesión fallidos.
Debes activar esto (y luego mantener tu teléfono alejado de los niños curiosos y las mascotas).

Debes desactivar las notificaciones de la pantalla de desbloqueo, o al menos desactivarlas desde las apps que contienen información delicada.
También debes desactivar el acceso a las apps desde la pantalla de desbloqueo.

Si tienes el cifrado de dispositivo activado en tu teléfono, tus datos son más protegidos cuando el teléfono está apagado (o se ha encendido pero aun no se ha introducido la contraseña de desbloqueo).
Después de desbloquear tu teléfono una vez, la protección de tus datos es menos fuerte.

Muchos activistas dejan activado el desbloqueo de huella dactilar porque es muy conveniente si se compara con escribir una contraseña 100 veces al día.
El hecho de que la conveniencia tiende a primar sobre la necesidad de mayor seguridad es otra razón para no mantener información delicada en tu teléfono.
Si tienes activado el desbloqueo de huella dactilar, puedes desactivarlo temporalmente manteniendo pulsado el botón de encendido.
Esto se puede hacer antes de interactuar con la policía, ir a la cama, o dejar tu teléfono sin vigilancia.

## Funciones inalámbricas

Es recomendable que desactives el wifi y el bluetooth cuando no se están usando.
Ambos pueden ser puntos de acceso vulnerables para el robo de datos o de identidad.
Y, además, aumentan la superficie de ataque disponible para los hackers que intentan entrar en tu teléfono.
Aunque los riesgos de dejarlos continuamente activados son mínimos, estas prácticas pueden añadir algunas pequeñas mejoras a la seguridad, y si no necesitas wifi o bluetooth, ¿por qué no hacerlo?

## Copias de seguridad

Los teléfonos inteligentes a menudo vienen con una función para hacer copias de seguridad en una cuenta de *Cloud* vinculada al teléfono (en Apple para iOS y en Google para Android).
Apple ha interrumpido anteriormente sus planes para cifrar las copias de seguridad en su iCloud tras presión del FBI, por lo que sus copias de seguridad no están cifradas.
Sin embargo, Google ofrece copias de seguridad cifradas de extremo a extremo que, tras revisiones externas, ofrecen sólidas garantías de privacidad por parte de las autoridades o del propio Google.
Además, algunas apps pueden tener sus propios servicios de copias de seguridad.
Por ejemplo, WhatsApp puede hacer copias de seguridad de tus conversaciones en sus servidores.

No recomendamos hacer una copia de seguridad en Apple, pero las copias de seguridad de Google son lo suficientemente seguras porque tu teléfono no debe contener evidencias incriminatorias de todos modos.
Dado que los datos enviados a terceros pueden perderse o destruirse incluso si no pueden ser recuperados por la policía, deberías considerar hacer una copia de seguridad de tus datos en un disco duro cifrado que tengas en casa o en algún lugar seguro.

## Apps de mensajería

Las apps de mensajería son alternativas más seguras a las llamadas telefónicas y los mensajes SMS.

## Cifrado

Las apps de chat de texto y voz ofrecen uno de dos tipos de cifrado.

**El cifrado cliente-servidor** es un tipo de cifrado en el que el canal entre un cliente (por ejemplo, tu teléfono) y el servidor es cifrado y protegido contra la interceptación, la manipulación, y la falsificación.
El mensaje se descifra y se almacena en el servidor.
Cuando el mensaje es solicitado por otro cliente (por ejemplo, el teléfono de tu amigo), se vuelve a cifrar para el tránsito y se envía.

**El cifrado de extremo a extremo **es un tipo de cifrado en el que los clientes generan claves criptográficas e intercambian sus partes públicas entre sí.
Los mensajes se cifran usando la clave pública del otro cliente y se envían a través del servidor.
El servidor trabaja como transmisor ciego porque los mensajes solo pueden ser descifrados por el otro cliente.

El cifrado de extremo a extremo solo significa que un servidor o alguien entre tu teléfono y el de tu compañero no puede leer o manipular un mensaje.
Un adversario todavía puede inferir información usando los
metadatos sobre el tamaño del mensaje, la hora de envío, el remitente y el destinatario.

Algunas apps de mensajería ofrecen un cifrado de extremo a extremo de *opt-in*, como Telegram con sus chats secretos. Sin embargo, esta función no está disponible para los grupos.
Otras apps tales como Signal o Wire cuentan con un cifrado de extremo a extremo obligatorio, al igual que iMessage y WhatsApp.[^p2p-chat].
Algunas apps como Element tienen activado por defecto el cifrado de extremo a extremo, pero puede desactivarse para la compatibilidad con clientes más antiguos.

[^p2p-chat]: Existen apps de chat más interesantes tales como Briar y Cwtch que protegen contra la fuga de los metadatos y poseen otras propiedades de seguridad interesantes, pero no se usan ampliamente.
Además, no están disponibles para iOS, lo cual impide a la mayoría de los grupos usarlas para la comunicación segura.

![Código QR y Huella Dactilar](./img/svg/qr-code.svg){width=80%}

La seguridad del cifrado de extremo a extremo depende de la verificación de las claves intercambiadas, la cual se realiza a menudo por escanear códigos QR que contienen una huella dactilar que es estadísticamente única para la clave generada.
Algunas apps requieren que verifiques solo una sola huella dactilar para todos los dispositivos, pero otras requieren que verifiques una huella dactilar para cada dispositivo.
Algunas apps te envían una notificación en la conversación para informarte cuando la huella dactilar de tu contacto se cambia, lo que podría significar algo nefasto.
Desafortunadamente, otras apps no lo hacen.
**Debes verificar **todas las huellas dactilares para todos los dispositivos, y si una huella dactilar se cambia, **debes volver a verificarla**.
Si no, toda tu seguridad podría ser anulada.
Además, algunas apps de chat no comparten los dispositivos que has verificado con tus otros dispositivos, y este mal diseño requiere que verifiques cada uno de los dispositivos de tus contactos desde cada uno de tus propios dispositivos.

## Uso

El mantra "solo usa Signal" se repite a menudo por los activistas, pero se basa en la falsa suposición de que todo el mundo tiene modelos de amenazas idénticos. En algunas regiones, el uso de Signal es bloqueado por los *firewalls* nacionales, o su uso es tan infrecuente que usarlo puede marcar a un usuario como sospechoso.
En Norteamérica y Europa, estas desventajas por lo general no existen.
Sin embargo, hay muchas quejas contra Signal tal que requiere un número de teléfono para registrarse, y esa lista de contactos es compartida con el servidor de manera semi-segura para permitir el descubrimiento de contactos y el intercambio de claves inicial.

Para la mayoría de las apps, al recibir un mensaje, es descifrado y almacenado en un texto plano en el dispositivo.
Algunas apps como Signal te permiten establecer una contraseña para evitar el acceso al mensaje mientras otra persona está usando tu teléfono, pero esta función no vuelve a cifrarlas de ninguna forma.
Si el cifrado de dispositivo es activado en tu dispositivo, se recupera parte de tu privacidad sobre estos mensajes, como se explicó anteriormente en la sección de "cifrado
de dispositivo."

Como los mensajes son almacenados en texto plano, y dado que podrían ser recuperados incluso con el cifrado de dispositivo, probablemente querrás activar los mensajes que desaparecen.
En algunas apps, una de las partes puede activar los mensajes que desaparecen para todos los miembros del chat.
En otras apps, cada una de las partes deben activar los mensajes que desaparecen para asegurar que todos los mensajes desaparezcan eventualmente.
Puede ser inconveniente tener activado los mensajes que desaparecen puesto que la búsqueda de una imagen, un archivo, o alguna decisión solo es posible hasta cierto punto, como dentro de una semana o un mes.
Sin embargo, esto es preferible a tener un registro de varios años de todo lo dicho y pensado y, en particular, todos los lugares que has estado.

Esto significa que debes preferir firmemente las apps de chat de texto y voz que tienen el cifrado de extremo a extremo obligatorio, a menos que haya una razón de seguridad convincente para no hacerlo.
Por último, debes verificar las claves antes de enviar mensajes, y probablemente deberías activar los mensajes que desaparecen.

## No "solo uses Signal"

Varias organizaciones de privacidad además de activistas preocupados hicieron un excelente trabajo de promover la adopción de Signal entre los activistas y también el público en general.
Posiblemente hicieron un trabajo *demasiado* bueno, considerando que muchas personas lo malinterpretaron como "si usan Signal, están totalmente seguros."
Esto ha llevado a que algunas personas hablen de cosas de las que absolutamente no deben hablar a través de medios electrónicos y luego suponen que está bien porque usaron Signal.
Todas las contramedidas de seguridad vienen con una serie de supuestos y entendimientos, y con eso, puede haber un riesgo aceptado o cosas que están fuera de nuestro alcance.
Signal es muy bueno en prevenir el uso por parte del estado de la vigilancia masiva para leer los mensajes.
Incluso oculta algunos pero no todos los metadatos.
Otras apps de chat tienen un modelo de amenazas más o menos similar.
Sin embargo, si tu teléfono se ve comprometido por el malware porque llamaste la atención o tuviste mala suerte, Signal no evitará que se lean tus comunicaciones.[^infiltrators]

[^infiltrators]: Además, algunas personas tienen prácticas de seguridad terribles, como unirse a muchos grandes grupos de chat de Signal y hablar de sus acciones sin investigar quién más está en el grupo.
No importa lo bueno que sea el cifrado si uno de los miembros del grupo es un infiltrado o un soplón.

![Editor de Métodos de Entrada y Candidatos de Pinyin](./img/bin/ime-pinyin-options.png){width=50%}

Para algunos idiomas, en particular los basados en los caracteres más que en las letras, se utiliza un Editor de Métodos de Entrada (IME) para convertir secuencias de letras latinas en los caracteres del idioma de destino.
Suelen ser aplicaciones de terceros.
Signal falla en advertir adecuadamente a los usuarios que usan IMEs sobre la posibilidad de que sus chats puedan ser leídos por el *software* y reportados al Estado antes de que los mensajes sean cifrados.

**Signal no es una garantía de la seguridad.** No lo trates como si la fuera.
Lo mismo aplica para cualquier app de chat cifrada de extremo a extremo.

Aunque tenemos fuertes críticas para Signal, esta crítica es motivada por la popularidad de Signal y las ideas erróneas al respecto; pero en el momento de escribir este texto, sigue siendo una de las pocas apps de chat cifradas que se puede confiar para obtener una seguridad sólida.

## Correo electrónico

Hay formas de hacer que la comunicación por correo electrónico sea más segura, pero el correo electrónico como protocolo y medio de comunicación no suele ser seguro para las comunicaciones privadas.
Los proveedores de correo electrónico que son amigables para los activistas (es decir, no Gmail/no Microsoft, etc.) no ofrecen ventajas de seguridad significativas contra la interceptación por parte de las autoridades o los hackers.
Al enviar un correo electrónico, algunas personas usan PGP o S/MIME, pero estas son difíciles de usar y en general tienen una pobre experiencia de usuario.
Dos personas que usan estos métodos de cifrado pueden tener una protección bastante buena contra la lectura de sus correos electrónicos, pero un clic por error puede enviar la historia entera de la conversación en texto plano, haciéndola así visible a las autoridades.
ProtonMail ha hecho audaces afirmaciones acerca del cifrado de sus correos y clientes, y muchos activistas han interpretado estas verdades a medias como que el uso de ProtonMail significa que todos sus correos son cifrados, pero este no es el caso.
Por lo general, debe evitarse el correo electrónico para la planificación y, sobre todo, para la comunicación segura.

Dicho esto, el correo electrónico mantiene su popularidad porque cada dispositivo puede enviar y recibir un correo electrónico, y algunas personas no usan los apps de chat.
Para organizar un sindicato de inquilinos o establecer turnos para un Infoshop, el correo electrónico probablemente está bien.
Si optas por usar email, debes asumir que la policía está leyendo todos los mensajes y reducir las conversaciones al mínimo.
No hables de la actividad ilegal.
No hables del drama de la escena que puede ser aprovechado por el Estado.

Por último, hay casos de uso legítimos en los que el correo electrónico y PGP se utilizan como último recurso para establecer un segundo canal más seguro para alguien en la clandestinidad.
En casos como este, los teléfonos todavía deben ser evitados, dado su rastreabilidad.

## Alias múltiples, teléfonos múltiples

Dependiendo de tu modelo de amenaza, puedes elegir mantener varios teléfonos vinculados a a tus varios alias.
Por ejemplo, podrías tener un teléfono vinculado a tu vida pública en el status quo con las cuentas de redes sociales que usas para conectar con tu familia y un segundo teléfono con una SIM separada y cuentas vinculadas a tu vida activista.
Esta separación de cuentas es parte de un proceso llamado compartimentación.

El principal beneficio es que el uso de diferentes dispositivos para cada uno de tus alias previene que errores del usuario o de programación expongan tu información privada.
Las aplicaciones de tu teléfono pueden tener comportamientos inesperados como enviar una invitación para conectar a todos tus contactos cuando descargas una nueva app de mensajería.
Puede que respondas por error a un post en redes sociales desde la cuenta equivocada.
Al hacer click en una dirección de email con la intención de usar uno de tus alias, el sistema operativo de tu teléfono quizá te lleve a escribir un email en el cliente de email por defecto con un alias diferente.

El segundo beneficio es que el dispositivo que uses para activismo puede ser mínimo y solo usarlo para comunicaciones seguras.
Cada aplicación que instalas es una posible ruta de entrada para malware, por lo que si tu teléfono apenas tiene un sistema operativo y dos aplicaciones de chat es más difícil de comprometer.

Usar diferentes teléfonos por sí mismo no evita que las fuerzas de la ley sean capaces de vincular tus diferentes alias.
Si llevas los teléfonos al mismo tiempo o bien los usas en los mismos lugares podrían ser vinculados.

Como alternativa a utilizar múltiples teléfonos puedes reducir el riesgo de filtrar datos por error o comportamiento inesperado creando múltiples perfiles en tu dispositivo Android.
Esto no te protegerá contra el malware, pero ofrece alguna protección.

Uno de los casos de uso más comunes de los teléfonos múltiples es para organizar un sindicato.
Algunas empresas obligan a instalar aplicaciones para la gestión remota como forma de proteger la propiedad intelectual corporativa o mitigar las brechas de seguridad.
Estas aplicaciones son software espía y pueden controlar completamente tu teléfono.
Aparte de esto, muchas empresas requieren una aplicación de chat específica para comunicarse.
Deberías evitar organizarte en dispositivos de la empresa o con su software espía instalado, además de evitar el chat de la empresa para intentar organizar un sindicato.

## Teléfonos para manis, desechables y quemables

Mucha gente entiende la importancia de sus teléfonos y saben que pueden ser rastreados por ellos o que su pérdida o compromiso pueden ser devastadores.
Los activistas (y no solo ellos) usan toda una serie de tácticas para ayudar a reducir su riesgo incluso si no pueden definir bien ese riesgo o por qué sus contramedidas funcionan.

Algunas personas tienen teléfonos para manis o desechables que se llevan a acciones o cuando cruzan fronteras.
Estos aparatos tienen el mínimo de datos privados en su interior y se consideran inseguros (debido a la posible instalación de malware) si las fuerzas de seguridad han tenido acceso a ellos.
Estos teléfonos no se usan con fines de anonimato.
Podrían compartir SIM con el teléfono habitual del usuario y podrían usarse de forma que la geolocalización los vincule con la vivienda del usuario.
Los teléfonos para manis tienen menos datos y cuentas disponibles para la policía si ésta los captura.
No hay necesidad de que un teléfono para manis o desechable sea un teléfono simple.
En muchos casos son smartphones porque esto permite al usuario usar mapas y comunicaciones encriptadas de extremo a extremo.

Algunos activistas usan erróneamente la frase "teléfono quemable" para describir teléfonos de mani, desechables o cualquier teléfono simple.[^burner-phone]
Un teléfono quemable toma el nombre del hecho de que es de un sólo uso y después es destruido.
Son el tipo de teléfono adecuado cuando el usuario necesita tener comunicación móvil durante el tipo de acción que lleva a una enorme y certera investigación como respuesta.

[^burner-phone]: Parece que la gente usa la frase "teléfono quemable" porque suena super ilegal y mega criminal y no porque estén describiendo realmente las propiedades de un teléfono quemable.

Para que un teléfono sea quemable necesita cumplir estos criterios:

1. El teléfono debe ser comprado[^burner-purchase] utilizando dinero en efectivo.
1. La SIM usada en el telefono quemable debe ser comprada usando dinero en efectivo.
1. El teléfono y la SIM deben ser compradas por un usuario que no tenga otros teléfonos o aparatos rastreables encima en el momento de la compra.
1. El teléfono y la SIM sólo deben ser usados el uno con el otro.
1. El teléfono nunca debe ser llevado a localizaciones asociadas con el usuario a no ser que esté apagado y dentro de una bolsa de Faraday.
1. El teléfono nunca debe ser usado en presencia de otros teléfonos que no sean quemables u otros aparatos que puedan ser rastreados hasta el usuario o sus socios.
1. Cualquier cuenta en el teléfono debe hacerse anónimamente, debe usarse sólo en ese teléfono y no volver a usarse jamás.
1. El teléfono debe ser usado exactamente en una sola acción.
1. El teléfono debe contactar únicamente con otros teléfonos quemables o terceras partes no relaccionadas (por ejemplo una oficina o adversario objetivo de la acción).
1. El teléfono y la SIM deben ser apagados después de la acción y destruidos inmediatamente.

[^burner-purchase]: El robo de teléfonos con una SIM activada generalmente es poco recomendable ya que cada robo crea una localización que puede ser vinculada con la acción, los teléfonos tal vez no puedan ser desbloqueados y el dueño quizá tiene el aparato añadido a una lista de denegación de servicio y no podrían ser usados para hacer llamadas o usar datos.

Un factor que complica las cosas es que algunos teléfonos o tarjetas SIM necesitan activarse llamando a un número específico o accediendo a la web del proveedor.
A veces estas webs bloquean conexiones a través de Tor.
Usar un teléfono no quemable para activar la SIM es, obviamente, una vulneración de su seguridad.
Quizá necesites buscar un teléfono de pago o usar la ingeniería social para hacer que un extraño en una estación de tren te deje su móvil por unos minutos.

Cuando decimos que un teléfono quemable puede ser usado para una acción quiere decir "una sucesión de actividades acotadas en el tiempo."
Esto puede significar una acción directa que tiene lugar a lo largo de dos horas, pero también la planificación y coordinación en el mes anterior a una acción además de la acción en si.

Si se tiene especial cuidado, un grupo de afinidad cerrado puede reutilizar sus teléfonos quemables para acciones recurrentes.
En este caso los teléfonos tienen que ser usados de forma de lotes cerrados que no se sobrepongan unos sobre otros en los diferentes ciclos.

Una norma que no es obligatoria, pero sí bastante recomendable, es que los teléfonos quemables no deberían comprarse inmediatamente antes de una acción.
Esto crea la posibilidad adicional de que los vídeos grabados por las cámaras de seguridad estén aún accesibles en el momento de la acción.

Intentar ofuscar la existencia de un ciclo cerrado entre los teléfonos puede ayudar a prevenir la detección del grupo de afinidad.
Uno de los pasos a seguir sería no activarlos todos en poco tiempo.
La activación gradual es menos detectable cuando el Estado analiza los datos.
También hacer unas cuantas llamadas desde localizaciones aleatorias a números que la gente llama comúnmente, pero **no hablar** si alguien contesta, o llamar a números con largos períodos de espera como bancos o aseguradoras, llamar a tiendas locales antes de que abran o después de que cierren...
Aunque todas estas llamadas falsas tal vez sean innecesarias ya que muchos usuarios en algunas regiones nunca hacen llamadas y simplemente usan su plan de datos para todo.

Debido al cuidado con el que un teléfono quemable debe ser adquirido y usado, es muy poco probable que valga la pena usar uno.
Si creéis que vuestra acción necesita un teléfono quemable, seguramente deberíais encontrar la manera de hacer la acción sin usar teléfonos en absoluto.
Para dejar claro a otra gente que los teléfonos quemables deben tener estas propiedades, evita usar el término "teléfono quemable" y utiliza teléfono para manis o teléfono desechable en su lugar cuando sea adecuado.

## Degradación elegante

Este zine se centra principalmente en debatir las características ideales del uso de teléfonos seguro, pero a menudo estas ideas no se pueden seguir.
Un ejemplo es cuando os organizáis con gente que no se puede permitir un smartphone.
Conseguir teléfonos simples y baratos para organizar una acción, o incluso para coordinar reuniones regulares, puede ser mas fácil y económicamente gestionable que hacer lo mismo con smartphones.
Por desgracia, la falta de aplicaciones encriptadas tanto para voz como para chat significa un incremento de la vigilancia para vuestras comunicaciones.

Para prevenir que el Estado pueda adquirir demasiada información sobre vuestras acciones tendréis que confiar en soluciones humanas antes que en soluciones técnicas.
Un acuerdo acerca de discutir la hora y el lugar de las reuniones siempre con la mínima cantidad de detalles e información puede reducir la información reunida por el Estado al mínimo.
Un libro de códigos para reemplazar frases frecuentes usadas al organizarse por códigos aleatorios e inocuos puede crear confusión si alguien intenta investigar.
Además, el uso de códigos puede evitar que los sistemas automáticos alerten a las autoridades.

El uso de patrones como estos os permite degradar vuestra seguridad de forma elegante de alta seguridad a baja seguridad sin exponeros completamente a la vigilancia y la represión del Estado.
Estos métodos requieren gran cuidado, pero son factibles.
