<!-- vim: set spelllang=es : -->

# Haciendo un plan

No podemos saber a qué tipo de amenaza te enfrentas, ni tampoco detenernos en cada pequeño matiz de cada región y situación.
Lo que sí podemos hacer es una lista de recomendaciones que son generalmente aplicables.
Cuando las leas tienes que considerar qué es más práctico para tí.
Qué puedes hacer realmente? Y qué hará la gente de tu círculo?
Tu nuevo plan no tiene que ser perfecto, solo tiene que ser mejor que lo que sea que haces ahora.
Si esto significa comprometerse en temas de seguridad para poder seguir organizándote, quizá tengas que hacerlo.
Pero al mismo tiempo no dejes que la poca seguridad de otras personas te ponga en peligro.
Encuentra un equilibrio.

Esto no es para nada una lista exhaustiva, pero son formas de desarrollar Seguridad Operacional personal y cultura de la seguridad grupal:

- Usa un smartphone ya que son más seguros que los móviles simples contra la mayoría de las amenazas a las que se enfrentan los activistas.
- No lleves tu móvil a actividades que puedan interesar a la policía, especialmente a protestas que puedan acabar en alborotos.
- Usa preferentemente aplicaciones de mensajería con cifrado de extremo a extremo para comunicarte, activa la desaparición de mensajes y evita el email sin cifrar.
- Usa una contraseña para desbloquear tu teléfono y activa la encriptación del dispositivo.
- Desactiva el desbloqueo por huella dactilar de tu teléfono antes de ir a dormir o dejarlo por ahí.
- Haz una copia de seguridad de las fotos y otros datos sensibles de tu teléfono a un dispositivo encriptado y elimínalos del teléfono.
- Elimina los datos antiguos: Mensajes directos, grupos de chat, emails, eventos en calendarios, etc.
- Sal de los grupos de chat que ya no necesites y elimina a los miembros de grupos que no sean necesarios.
- Habitúate a dejar el teléfono en casa o apagarlo cuando vas a hacer pequeños recados o acciones para acostumbrarte a su ausencia.
- Empieza todas las reuniones estableciendo si los aparatos electrónicos están o no permitidos.
Si no, apágalos, recoléctalos y ponlos fuera del alcance de vuestra conversación.

Y más importante:

> **No envíes mensajes o hagas llamadas de voz donde hables de temas sensibles.
> No fotografíes o filmes cosas incriminatorias.
> No crees pruebas que puedan ser usadas contra ti o contra otros.**

## Sujeto a revisión

Lo que se escribe aquí, así como en el resto del fanzine, son recomendaciones.
Puede que no se apliquen a tu caso.
La seguridad digital, en concreto, puede dejar rastros especialmente llamativos.
Si Signal es muy poco común en tu región, su uso puede convertirte en un objetivo.
Las VPNs pueden ser criminalizadas.
El uso de Tor puede acarrearte una visita de la policía.
La presencia de aplicaciones de comunicación segura en tu teléfono puede convertir tu arresto en una desaparición.
Antes de descargar cualquier cosa investiga acerca de la represión en tu región para determinar si las recomendaciones que te proporcionamos te harán estar más seguro o si te pondrán más en riesgo.

## Alternativas

Es siempre más fácil decir "haz esto en vez de lo otro" en lugar de decir "no hagas eso" y, cuando estás intentando cambiar comportamientos o prácticas, proporcionar alternativas incrementa las posibilidades de que alguien deje aquel viejo e inseguro hábito.
Hay razones legítimas para tener teléfonos, y las alternativas pueden aliviar la carga de tener que dejar de usarlos o cambiar nuestros hábitos.

Las barreras a la hora de librarnos de los teléfonos son que la gente quiere tener, intercambiar y reunir información.
Un lápiz y un bloc de notas te permiten tener el orden del día de tu asamblea accesible de forma analógica.
También te permite tomar nota de nuevos contactos y, si eres avispado, puedes llevar a mano una copia de tu clave pública criptográfica para establecer un canal seguro de comunicación incluso sin llevar el móvil encima.
Un calendario de papel te permitirá agendar eventos.
Imprimir mapas del área de una acción hará que puedas orientarte durante la misma.
Si creas copias de papel de cualquier información, asegúrate de que te deshaces de ellas de manera adecuada para evitar dejar, literalmente, un rastro de papel de tus actividades.

## Imprevistos sin teléfono

Aunque tu plan puede que funcione hoy, hay que seguir pensando.
Puede que confíes mucho en tu teléfono para organizarte mientras aceptes los riesgos de seguridad, pero quizá llegue un momento en el que la represión o una catástrofe desactive vuestros teléfonos o el internet.
Es frecuente, en momentos represivos álgidos, que el Estado corte el servicio de telefonía móvil o internet de regiones enteras.
Si vuestra capacidad de organizarte y tu seguridad se basa en que casi todos tengáis teléfonos e internet funcional os estáis preparando para ciertos tipos de fracaso.
El boca a boca y el llamado sneakernet[^sneakernet] son planes B y vuestro plan necesita incorporar la posibilidad de que esta sea la única manera de mover información.

[^sneakernet]: *N. del T.*: Se denomina así a las formas de mover información físicamente mediante soportes de memoria como USBs, discos, etc. en lugar de a través de internet.
