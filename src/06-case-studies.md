<!-- vim: set spelllang=es -->

# Casos de estudio

Para concretar acerca de las discusiones anteriores, proporcionamos una serie de casos de estudio sacados de nuestras experiencias.
Algunos de ellos muestran individuos que ya tienen un modelo de amenaza más concreto y otros no.
Algunos están basados en leyendas urbanas y otros más en hechos verificables o conjeturas muy probables.
Donde hay errores, éstos se discuten.

## Caso 1: Planear asambleas para una acción semi-pública

### Escenario

Un colectivo planea una okupación que se mantendrá en secreto hasta que empiece, después de lo cual se hará pública en las redes sociales.
La planificación se lleva a cabo principalmente en reuniones presenciales en el centro social local.

### Supuestos

El colectivo supone que la policía está interesada en prevenir okupaciones y los activistas podrían estar bajo vigilancia.
Esta vigilancia incluye, pero no se limita a, malware usado por el estado que podría estar infectando los teléfonos de los participantes en la acción.

### Contramedidas

Para prevenir que el estado pueda grabar la asamblea usando los micrófonos de los teléfonos, los teléfonos se recolectan y se ponen en una bolsa de plástico sellada en la habitación contigua.

### Análisis

Es cierto que los teléfonos podrían haber sido infectados con un malware y también podría ser cierto que mover los teléfonos a otra habitación impide que sus micrófonos graben la conversación.
En cualquier caso, estamos suponiendo la efectividad de mover los teléfonos a la habitación contigua, algo que podría ser fácilmente comprobado si comenzamos a grabar con un teléfono dentro de una caja y después tenemos una conversación en voz alta en la habitación de al lado para verificar si se entiende lo que se ha grabado.
Si las voces son mínimamente reconocibles podrían recuperarse pedazos de conversación con software de edición de audio.

Si el centro social no se registra en busca de micrófonos regularmente las conversaciones pueden ser grabadas de todos modos.
Si el grupo u otros grupos que frecuentan el local están fuertemente vigilados las conversaciones pueden ser grabadas mediante micrófonos láser instalados en los edificios cercanos.

Si los individuos están bajo vigilancia pasiva, la existencia de una asamblea y sus asistentes podría ser revelada por la presencia recurrente del mismo grupo de teléfonos en la misma localización a la misma hora.

### Recomendaciones

Si los teléfonos se recolectan para prevenir la vigilancia, también deberían apagarse.
Debería haber un sonido ambiente ruidoso donde se coloquen los teléfonos para minimizar las probabilidades de que capturen audio de las conversaciones.

Si el grupo cree que pueden ser buscados por conspirar para cometer un crimen quizá quieran dejar sus teléfonos en casa o apagarlos antes de trasladarse a la asamblea.
Estas medidas pueden mejorarse incluso si no se llevan los mismos teléfonos a la acción en sí.

Si se desea una alta seguridad se puede reducir la probabilidad de vigilancia reuniéndose en localizaciones que no estén relacionadas con movimientos de liberación.
Si el grupo quiere reunirse en una localización central y conocida por conveniencia, se debería establecer al inicio de la reunión que sólo se discutirá de la acción actual (y nada más ilegal que eso).

## Caso 2: Cháchara Descubierta

### Escenario

Algunos miembros de un grupo de afinidad han quedado en un parque para socializar, no para planear una acción.
Sus teléfonos están presentes y encendidos, pero su cultura de la seguridad incluye no discutir acciones pasadas o intercambiar batallitas ya que éstas pueden contener información que los incrimine.

### Supuestos

El grupo ha asumido que la policía sólo quiere escuchar sus conversaciones si éstas van sobre actividades ilegales pasadas o futuras.
Han asumido que sus conversaciones diarias no son interesantes ni informativas.

### Contramedidas

El grupo no ha tomado ninguna contramedida contra la vigilancia de sus conversaciones.

### Análisis

Si el grupo no discute planes o acciones deliberadamente, entonces ningún micrófono podrá registrar lo que se planea.
En cualquier caso, las acciones planeadas y llevadas a cabo no es lo único en lo que el Estado está interesado.
Los chismorreos, dramas, intereses amorosos, lazos sociales e incluso las medidas que las personas y organizaciones de un gueto político toman contra otro son información valosa.
Esto podría permitir al Estado crear mapas sociales más precisos.
Si el Estado sospecha que un individuo estuvo involucrado en algo que están investigando, y saben que éste tiene cómplices, el uso de mapas sociales construidos a partir de pequeños pedazos de conversaciones casuales pueden ayudarles a reducir su lista de sospechosos o revelar los miembros de un grupo de afinidad.
Dichas charlas descubiertas pueden dar al Estado pistas acerca de quién se siente desplazado y resentido para que puedan intentar convertirlo en un informante.
Los pequeños conflictos pueden ser explotados y los ánimos caldeados pueden ser convertidos en feroces disputas.

### Recomendaciones

Hay una brecha generacional entre los activistas que se organizaban antes de la generalización de los teléfonos móviles y aquellos que empezaron a organizarse después de que éstos fueran ubicuos.
Hay, también, una brecha más entre quienes se organizaban usando teléfonos simples antes de que los smartphones fuesen populares y aquellos que se han organizado siempre en un mundo donde casi todos sus contactos tienen smartphones.
Esta brecha puede percibirse por la habilidad de hacer planes asumiendo que los demás no tendrán teléfonos, por ejemplo fijando lugares y horarios sin hacer cambios de última hora.

Como se ha mencionado anteriormente en este fanzine, los smartphones nos permiten comunicarnos instantáneamente y tener información ilimitada en mano en cualquier momento.
Esto tiene el coste de nuevas vías de vigilancia.
Los activistas deberían estar al tanto de que los teléfonos presentes en hogares, coches y ambientes sociales podrían estar reuniendo información de grupos sociales.
Si hiciésemos la recomendación de que los teléfonos deberían estar apagados más frecuentemente se reirían en nuestra cara por paranoicos o por la imposibilidad de seguir la recomendación.
La llamada democracia liberal proporciona la ilusión de que no vivimos bajo un estado policial represivo, pero aun así hay muchos casos de circulos sociales inofensivos y grupos activistas hackeados y espiados, por no mencionar los grupos más involucrados y radicales.

Nuestra sugerencia no es que nunca deberíamos llevar móviles encima, sino que queremos sugerir que todo el mundo tenga más conciencia del esfuerzo que invierte el Estado en espiar y de la utilidad de la información sacada de conversaciones casuales.
Podría llegar el día en que la represión se recrudezca y comencemos a sentir su presencia de manera más aguda.
Para prepararnos para ese tiempo y construir hábitos que nos permitan resistir a esa represión nuestra sugerencia es más moderada.
Practicas de alta seguridad comenzando desde ya.
Intentar organizar eventos sin móviles.
Cuando vas a hacer senderismo o quedas con alguien, incluso cuando vas a un pub, intenta convencer a todo el mundo de que dejen sus teléfonos en casa.
Acostúmbrate a su ausencia.
Siente la libertada de saber que no estás filtrando datos de localización al Estado y que nadie puede oir tus conversaciones excepto aquellos presentes.

## Caso 3: Okupación y teléfonos simples

### Escenario

Un grupo de activistas quieren okupar un edificio vacío con el objetivo de llamar la atención sobre las especulación inmobiliaria y, si la okupación tiene éxito, convertirlo en hogares gratis para personas locales que han sido deshauciados recientemente.
Un equipo estará en el edificio realizando la okupación, mientras que otros estarán sobre el terreno negociando con el Estado y escribiendo en las redes sociales.

### Supuestos

El equipo de okupación cree que la policía podría averiguar sus identidades viendo qué teléfonos se están comunicando desde el interior del edificio, e incluso si no fuesen arrestados o perseguidos por esta acción, ese conocimiento podría ser usado contra ellos en el futuro.

### Contramedidas

Para reducir la probabilidad de que se descubran sus identidades aunque no sean detenidos durante la acción, el equipo de okupación ha escogido no llevar sus teléfonos personales.
Únicamente llevarán un teléfono "quemable" para comunicarse con el equipo de negociación y poder participar de las decisiones, enviar posts al equipo de redes sociales y tener sensación de seguridad en lugar de permanecer aislados hasta el final de la acción.
Usarán un teléfono con una SIM que no está registrada a nombre de ninguno de ellos para así permanecer anónimos.

### Análisis

El grupo está acertado no llevando sus teléfonos personales al edificio que están okupando ya que podría ser usado para identificarles.
La policía podría hacerlo mirando qué teléfonos están en el edificio y a nombre de quién están registrados o dónde tienden a pasar más tiempo (Por ejemplo, cuando el usuario está en casa durmiendo).
El grupo se equivoca en llamar al teléfono "quemable" ya que su uso continuado puede ser utilizado para vincularlo con el grupo y sus integrantes.
Este teléfono puede ser descrito más precisamente como un teléfono para manis.
Ya que hay miembros del grupo que permanecen fuera del edificio, sin enmascarar, la identidad del grupo se conoce incluso si no todas las identidades de la okupación son conocidas.
Si el teléfono es un "quemable" personal de uno de los activistas y éste se ha encendido en su casa, ésto puede ser usado para probar que el activista estaba dentro del edificio o estaba involucrado.

El grupo ha pasado por alto las implicaciones de seguridad de usar un teléfono simple para comunicarse con el equipo de negociaciones.
La policía podría desplegar un receptor IMSI para poder leer los SMS que se intercambian entre el equipo de okupación y el de negociación.
Esto podría dar ventaja a la policía en las negociaciones o la oportunidad de aprovecharse de las divisiones dentro del grupo para forzar un desalojo más fácilmente.

En cualquier caso, si la policía quiere invertir este nivel de esfuerzo en rastrear los individuos basándose en los teléfonos presentes en la okupación es que probablemente hay motivos para pensar que la okupación en sí misma no será viable por la fuerte presión de la "ley y el orden".

### Recomendaciones

Las razones de que el equipo de okupación quisiera llevar un teléfono simple al interior del edificio eran legítimas, pero deberían haber usado un teléfono para manis con una cuenta de un sólo uso que creasen en una app de chat encriptado.
Esta cuenta debería comunicarse únicamente con una cuenta anónima perteneciente a los equipos del exterior para prevenir filtrar las redes sociales del grupo si el teléfono es confiscado o el desarrollador de la app está guardando datos de esas cuentas que después puede ser requerida por un juzgado.

## Caso 4: Teléfono simple + Signal de Escritorio

### Escenario

Ruben es un activista involucrado en un grupo que cree que está bajo vigilancia activa debido a su carácter antigubernamental.
Para minimizar el rastreo de las agencias de inteligencia y policía local utiliza un teléfono básico con una SIM cuando está por ahí.
Algunos debates con su grupo son más delicados y necesitan una app de mensajería encriptada, para lo cual han elegido Signal.
Signal requiere de un registro con un número de teléfono y sólo generará las claves de encriptación iniciales a través de la aplicación de Android o iOS.
Para hacer que la aplicación de escritorio de Signal funcione en su portátil, Ruben ha usado la SIM de su teléfono básico en el smartphone de un amigo para poner en marcha la app de Signal y poder vincularla con su aplicación de escritorio.
Después ha cerrado la sesión de su cuenta en el teléfono de su amigo.

### Supuestos

La decisión de no llevar un smartphone se basa en la creencia de que los smartphones son más fácilmente rastreables que los teléfonos básicos.
Ruben también supone que Signal es más seguro que las llamadas de teléfono o SMSs, así que usa signal para algunas de sus comunicaciones.

### Contramedidas

La decisión de Ruben de usar un teléfono básico tiene la intención de minimizar el rastreo de la localización de un smartphone.
Su decisión de usar Signal desktop pretende prevenir la interceptación de sus mensages sensibles con sus compañeros.

### Análisis

La localización de Ruben es prácticamente igual de rastreable usando un smartphone que usando un teléfono básico.
Sus comunicaciones son más inseguras porque no tiene la posibilidad de enviar o recibir mensajes de "emergencia" con sus compañeros usando su teléfono básico y, si lo hace, serán interceptados y almacenados por el Estado.
Sus contramedidas contra el espionaje han creado una barrera entre el y su grupo, además de no proporcionarle una mayor seguridad ante las amenazas que enfrenta.

### Recomendaciones

Ruben debería usar su propio smartphone para comunicarse en general.
Si hay veces en las que necesita esconder su ubicación o sus conversaciones debería dejar el móvil en casa.

## Caso 5: Planear sin móviles

### Escenario

Los miembros de un grupo de afinidad han estado involucrados en movimientos de liberación durante el tiempo suficiente para ser conocidos por el Estado.
Actualmente están planeando Algo Grande.
Se han prohibido hablar sobre el tema cerca de aparatos electrónicos y únicamente lo hacen en persona.

### Supuestos

Dan por supuesto que el Estado hará lo que sea necesario para prevenir su acción e incluso más para investigarla si llega a ocurrir.
Dan por hecho que es posible que sus aparatos electrónicos hayan sido comprometidos por malware del Estado.
También dan por hecho que, incluso en ausencia de pruebas, estarán en la lista de principales sospechosos de la acción, por lo que su seguridad operacional necesita ser afinada.

### Contramedidas

Debido a la posibilidad de malware, están tratando sus aparatos como menos que fiables.
Por la posibilidad de ser el blanco de una investigación no discuten sobre la acción en sus hogares, vehículos o centros sociales conocidos u otros espacios ligados a movimientos de liberación.
Para ayudar a reducir los metadatos que les vinculan entre ellos apagan sus teléfonos antes de llegar a los lugares de reunión y los vuelven a encender cuando se han ido.

### Análisis

El grupo está en lo cierto cuando supone que podrían estar siendo vigilados y también al tratar sus teléfonos como a chivatos.
Apagar los teléfonos disminuye la posibilidad de que un malware use su micrófono para espiarles y crea cierta confusión sobre sus localizaciones durante las reuniones.
Pero esta ausencia de información podría resultar anormal en comparación con su uso habitual del móvil y todos sus móviles desapareciendo prácticamente al mismo tiempo cerca de una ubicación podría ser una pista para el Estado de que durante esos periodos algo interesante está ocurriendo.
Esto podría suponer un incentivo para aumentar la vigilancia, como por ejemplo poniendo micros en la zona (si se reúnen siempre en el mismo lugar) o enviar a un secreta con un micrófono a seguirles en los bares o restaurantes donde se reúnen.
Es más, si un miembro del grupo cae pero no dice nada durante el interrogatorio, la policía podría mirar en su teléfono si existen pistas sospechosas.
La policía podría reunir la información preguntándose: En el momento en que se apagó el teléfono, qué otros teléfonos se apagaron cerca de él? Y qué estaban haciendo los teléfonos de otros sospechosos en ese momento? Esto podría revelar el resto de los miembros del grupo de afinidad o proporcionar pruebas que apoyen que los miembros del grupo de afinidad eran cómplices.
Es posible que la policía no piense en estas preguntas o no formen parte de sus operaciones habituales, pero es mejor no dejar ningún rastro.

### Recomendaciones

Ya que se están anticipando a la vigilancia y métodos para investigar sus actividades, deberían dejar todos sus aparatos en casa y elegir localizaciones al azar para sus reuniones que sean o bien muy ruidosas, o bien muy aisladas.

## Caso 6: Teléfonos en acciones masivas

### Escenario

Isa es una activista que principalmente acude a grandes manifestaciones y, aunque ella misma no es muy radical, tiene algunos amigos que sí lo son y generalmente se entera de las cosas que hacen.
Los fascistas han planeado una marcha y Isa, junto con algunos amigos, van a ir a unirse a las masas que esperan bloquear la ruta planeada.
Para contactar con sus amigos y tener información al minuto sobre el bloqueo o cambio de rutas Isa va a llevar su teléfono habitual (el único que tiene).

### Supuestos

Isa no está preocupada por si le arrestan porque en acciones similares en el pasado, con una gran cantidad de gente que no aparenta ser el clásico antifa bloqueando las calles, la policía sólo les aparta a la vez que reconduce la marcha fascista.
Ella no piensa en que si es arrestada mirarán en su móvil, ya sea de forma legal o ilegal.
Tampoco está concienciada sobre los datos de localización de su móvil.

### Contramedidas

Isa no ha tomado contramedidas contra la recolección de datos de localización de su teléfono o la confiscación de éste.

### Análisis

En acciones masivas la policía puede utilizar rastreadores IMSI para ver quién ha asistido a las protestas y construir perfiles.
Estos datos de localización podrían usarse para perseguir a gente por disturbios incluso si los cargos no suponen penas de prisión.

Si detienen a Isa, lo cual podría ocurrir si el bloqueo no es muy grande o es una de las personas con la mala suerte de ser pilladas en las calles, podrían registrar su móvil.
A partir de aquí la policía podría aprender de sus redes sociales o las actividades de sus amigos más externos.
Esto puede ponerles en peligro más que a ella misma.

### Recomendaciones

Incluso si Isa no se anticipa a su detención, debería andar con más cuidado con su teléfono.
Podría quedar con sus amigos en localizaciones fijas algún tiempo antes de la mani para poder evitar traer sus teléfonos o, si realmente quieren tener información en tiempo real, solo una persona del grupo debería llevar un teléfono.
Ser cuidadosa con su teléfono puede proteger a su círculo más externo de amigos radicales que quizá están involucrados en medios de resistencia militante al fascismo.

En cualquier caso, las probabilidades de que ocurran cualquiera de estas cosas es baja y el beneficio esperado de llevar un teléfono es alto.
Eso hace que en este caso esté "bien" que Isa lleve su teléfono... hasta que, de repente, no lo esté.

## Caso 7: Comunicaciones y planes generales

### Escenario

Un colectivo organiza protestas legales y reparte folletos promoviendo alternativas verdes y ecológicas al status quo, como hacerse vegano, inversión en infraestructura ciclista, disminución de la dependencia del automóvil...
Utilizan una lista de mail alojada en un servidor proporcionado por un grupo de activistas tecnológicos local.

### Supuestos

El colectivo asume que la policía está interesada en los activistas en general, pero que el colectivo en sí mismo no está siendo amenazado específicamente.
Saben que los *trolls* locales gustan de acosar a los "comunistas hipis."
También que hay otras organizaciones verdes militantes más en su misma región, y que los miembros de su colectivo quizá estén en todo tipo de otros grupos.

### Contramedidas

El colectivo quiere evitar el acoso, así que mantienen su lista de correo privada y por invitación.
Quieren evitar ser rastreados por las grandes compañías de email, por lo que alojan su propio servicio de email.

### Análisis

Las listas de email son bastante populares porque todo el mundo tiene acceso al email, mientras que hay muchas aplicaciones de chat diferentes y no todo el mundo usa las mismas, por lo que los colectivos tienden a continuar usando listas de correo.
Frecuentemente la gente dice no tener suficiente espacio en sus teléfonos para más aplicaciones.
Algunos miembros de colectivos tienen pocas habilidades técnicas y no quieren aprender a usar otras aplicaciones, por lo que, a veces, el email es inevitable.

Los ecoactivistas de todo el mundo, incluyendo aquellos de las llamadas *democracias* occidentales, son un objetivo específico de vigilancia, incluso cuando no se involucran en acción directa.
Autoalojar el email quizá disminuya el riesgo de vigilancia corporativa, pero siempre hay algún resquicio por donde se pueden filtrar datos.
La policía podría ahorrarse tratar con el grupo de técnicos que gestionan el correo (y que quizá avisarían a los activistas incluso en el caso de tener una orden de registro) yendo directamente a por la empresa que aloja el servidor.
Además, los técnicos quizá no tengan la competencia técnica de las grandes compañías de email para proteger el servidor o descubrir si ha sido hackeado por trolls o el Estado.

### Recomendaciones

Si el espacio en el teléfono es un problema, los activistas deberían hacer una backup de sus fotos y vídeos para luego borrarlos y hacer espacio.
Esto es generalmente una buena práctica para ayudar a conservar los datos si el móvil se daña o pierde.

Lo ideal sería que el colectivo pasase a usar una aplicación de chat encriptada, pero si continúan usando el email debería ser sólo para los detalles más básicos como las horas y lugares de sus actividades.
Planes, debates internos y otras discusiones importantes deberían permanecer fuera del email ya que esta información puede dar al Estado grandes pistas sobre la situación del colectivo que pueden usarse en su contra.

## Caso 8: Raves Underground

### Escenario

Un colectivo planea raves al aire libre durante la pandemia del coronavirus.
Piden que la gente lleve máscaras y creen que con esto bastará para contener el virus.
La policía tiene restricciones para las reuniones de gente (excepto para ir a trabajar o a cualquier otra cosa que mantenga la máquina del capital engrasada).

### Supuestos

El estado ha hecho esfuerzos para disolver reuniones masivas (únicamente las de clase baja y marginal, por supuesto), pero no van a andar buscando pruebas o pistas de este tipo de reuniones.
Tienen la capacidad de reunir datos de localización de los teléfonos en tiempo real que podrían usar para detectar varios cientos de personas en una localización remota.
La Policía tiene informantes que escuchan cosas como ésta, y otra gente simplemente disfruta jodiendo a otros si oyen algo que no les gusta.

### Contramedidas

La rave no se ha publicado en las redes sociales y se solicita información antes de enviar los datos del evento a alguien mediante canales seguros.
En la info se pide a la gente que pongan sus teléfonos en modo avión cuando se acerquen a la localización.

### Análisis

No publicitar el evento es, obviamente, un paso correcto para evitar que la policía se entere.
Pedir a la gente que sólo difundan la info a contactos de confianza mediante canales seguros también es una buena forma de reducir el riesgo, pero sólo es necesaria una persona que envíe una versión abreviada del mensaje con el lugar y la hora para que éste aviso se pierda.
Incluso sabiendo esto, es un riesgo que el colectivo tiene que aceptar.

### Recomendaciones

Hay poco que el colectivo pueda hacer para evitar que la gente llegue al evento con sus teléonos encendidos, así como poco pueden hacer para asegurarse que el mensaje permanece sólo en las partes seguras y de confianza de la web.
Esto es un problema difícil en la cultura de la seguridad porque la falta de seguridad de una parte de los individuos puede comprometer al grupo por completo, especialmente cuando el beneficio individual de mantener el teléfono encendido es alto, mientras que el riesgo para el individuo es bajo.
Los organizadores que han creado el evento y cedido el equipo son los que tienen más posibilidades de afrontar las consecuencias.
Si la muchedumbre se disuelve durante una redada ellos se enfrentarán a las consecuencias con toda seguridad.
Lo mejor que el colectivo puede hacer es intentar utilizar la vergüenza y la presión social antes y durante el evento para convencer a la gente de que sus acciones podrían arruinar la rave para todos.

## Caso 9: Lidiando con un eslabón débil

### Escenario

Un grupo de afinidad va a por los nazis que acosan a la gente de su comunidad.
Tienen el acuerdo de no llevar teléfonos a sus acciones nocturnas.
Felix, uno de sus miembros más activos, cree que esto es un comportamiento paranoico y rechaza dejar el suyo en casa.

### Supuestos

El grupo ha asumido que el Estado quizá use los datos de localización de sus teléfonos para investigar sus actividades.
También son conscientes de que el comportamiento de Felix, llevando su teléfono, les pone en peligro.

### Contramedidas

Para evitar que Felix les ponga el peligro han parado sus actividades hasta que puedan llegar a un acuerdo con él.

### Análisis

Las acciones de Felix ponen en riesgo al grupo y el grupo tiene razón cuando considera que no deberían dejarle participar en sus acciones.
Si el grupo al completo interrumpe sus actividades el daño podría volver a su comunidad.
Y los riesgos de ser arrestados por culpa de un teléfono podría ser muy bajo dependiendo de cómo la policía investigue estos asuntos en dicha región.

### Recomendaciones

El grupo de afinidad podría crear un subgrupo de miembros quienes estén de acuerdo en no llevar teléfonos a las acciones y continuar su trabajo.
En paralelo, podrían trabajar con Felix para hacerle entender cómo y porqué llevar su teléfono crea riesgos innecesarios.
Podrían debatir con el que sus acciones les causan incomodidad y que sus acciones no sólo le afectan a él.
El grupo podría ser capaz de seguir siendo camaradas de Felix, pero podría verse obligado a excluirlo de acciones secretas si el rechaza dejar su teléfono en casa.
