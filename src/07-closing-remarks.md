<!-- vim: set spelllang=es : -->

# Notas Finales

La tecnología no es ni buena ni mala (al menos la mayoría).
No es totalmente liberadora ni opresiva.
Nueva tecnología crea nuevas oportunidades mientras cierra otras.
Con los teléfonos sucede lo mismo.
Tener acceso a comunicaciones instantáneas y un vasto conocimiento en nuestro bolsillo es un poder tremendo, pero viene con el coste de una vigilancia mayor.

Podrías pensar que el Estado no te está vigilando, pero si estásinvolucrado en movimientos sociales de liberación, aunque sea vagamente, seguramente lo hace.
Protegerte a tí mismo puede proteger a tus amigos,
familia o compañeros que están más metidos que tú en el movimiento.
Podrías pensar que el Estado hackea tu teléfono para escuchar tu asamblea semanal de la cooperativa de vivienda, pero seguramente no lo hace.
El máximo de seguridad todo el tiempo es insostenible y exigirlo es una mala idea.

Después de leer esto podrías tener la tentación de decir "pero me rastrearán de todos modos."
La creencia de que cualquier nivel de seguridad contra las amenazas externas es imposible se llama *nihilismo de la seguridad*.
La gente que se siente así a menudo toma dos caminos diferentes.
Pueden creer que ninguna contramedida funciona, así que continúan actuando sin tomar ningún tipo de precaución, creando una profecía de autocumplimiento que concluye con su detención.
O bien pueden creer en la supremacía del Estado y caer paralizados con la inacción.
La represión no funciona únicamente mediante el palo que nos azota, sino también mediante el miedo a ese castigo y la consecuente inacción autoimpuesta.

Cualquier paso que des puede protegerte, y muchos son tan sencillos que podrías empezar a aplicarlos ahora mismo.
Empezando por lo más fácil, puedes evitar el espionaje usando aplicaciones básicas encriptadas para enviar mensajes y dejando tu móvil en casa durante manifestaciones o acciones directas.
Cada paso que des más allá de éstos requerirá que tus adversarios hagan más esfuerzos si quieren vigilar o interrumpir tus actividades.
Tiempo y recursos son limitados, incluso para las grandes agencias de inteligencia.
Los humanos cometemos errores y los ordenadores se rompen.
Tus adversarios no son infalibles y puedes disminuir significativamente la cantidad de datos que pueden capturar y que tipo de pistas pueden atisbar a partir de ellos.

Además, el Estado no siempre usa el máximo teórico de métodos de vigilancia a su disposición.
Sólo porque sea posible para el Estado hackear tu teléfono o rastrearlo no significa que vayan a hacerlo para cogerte colándote a los parques después de la hora de cierre.
Incluso en casos en los que el Estado quiere utilizar el máximo de espionaje puede que lo haga de forma inepta.
Vuestro modelo de amenaza debería contar con una respuesta realista y esperable de vuestros adversarios dado su conocimiento de vuestras acciones.

Aprende acerca de cómo la policía, los fascistas y otros adversarios en tu área operan, e idea un modelo de amenaza para tí y tu grupo.
Discútelo bien con tus compañeros.
Comienza con pequeñas muestras de conocimiento de Seguridad Operativa y conviértelo en cultura de la seguridad.
Adopta conocimiento y prácticas compartidas que lleven a una mayor seguridad contra las amenazas que podríais enfrentar.
Dad pasos concretos, pero de forma pragmática.
Empezad poco a poco con sólo unas cosas nuevas cada vez hasta que las normalicéis, y luego construid a partir de ahí.
Un plan sólo es bueno si puedes llevarlo a cabo, e intentar hacer muchos cambios grandes apresuradamente tiende a sobrepasar y ser frustrante.
La mayoría de los planes exitosos se aplican incrementalmente.

Cuidado con las leyendas urbanas.
Los espacios activistas están plagados de ellas y la seguridad no es una excepción.
Preguntad *Cómo?* Y *Por qué?*
Cuando la gente diga cosas sobre seguridad o contramedidas.
Basad vuestro modelo de amenaza y vuestro plan de seguridad en hechos verificables o, al menos, en conjeturas muy probables con pruebas que las apoyen.

Usad este conocimiento para protegeros mientras redibujáis el mundo.
